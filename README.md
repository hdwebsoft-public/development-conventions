# Presentative Rules of README - HDWEBSOFT
The README file basically has the following sections:

## Title / Named for Project (*)
## Intro / Brief Description (*)
## Prerequisites (*)

Please list down any prerequisite knowledge or tools that anyone who wants to use the project might need before starting:
  + Computer equipment
  + Operating systems
  + Technologies
  + Programming languages
  + Etc.
 
_E.g._ 

 _Before you continue, ensure you meet the following requirements:_ 
- [x] _You have installed the latest version of Ruby._
- [x] _You are using a Linux or Mac OS machine. Windows is not currently supported._
- [x] _You have a basic understanding of graph theory._

## Installation / Setting Guidelines (*)

### Step 1:

(TBD)

### Step 2:

(TBD)

## References(*)

  _E.g._ 
  Link: https://github.com/othneildrew/Best-README-Template?tab=readme-ov-file)

## Additional Information

Depending on the specifics of the project, this position can flexibly choose to present 01 or several sections as follows:

### - Usage

### - Solution

Solutions to common errors and answers to frequently questions.

### - Licenses

### - Source code ownership

_E.g._

_This source code is the exclusive property of HDWEBSOFT COMPANY and is protected by copyright and other intellectual property laws. It contains confidential and proprietary information, including trade secrets, which are not intended for public disclosure. Unauthorized use, reproduction, distribution, or modification of this code is strictly prohibited and may result in legal action._

## Support / Contact (*)

Note contact information of the support team.

_E.g._
  - _Leader_ <leader@example.com>
  - _Sublead_ <sublead@example.com>
  - _Developer_ <developer@example.com>

## Acknowledgements

This is where you write down your thanks, articles or anything else that inspired or helped you complete this project.

_E.g._

_Thank you, Mr. Dat and Mr. Vu, for building this process to help HDWEBSOFT projects have their own specifically conventions. And thank Mrs. Nga for finallizing all of the items and letting them be implemented._

# Notes

- **(*)**: These are importance items to fill.
- **FEEL FREE** to add more sections to your README.md or learn from a lot of open-source GitHub or GitLab repositories.
